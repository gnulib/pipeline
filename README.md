# GNU Continous Integration Pipeline

Build and test your GNU-style project using [GitLab
CI/CD](https://docs.gitlab.com/ee/ci/).

We support GNU-like (`./configure` and `make`) projects with a
[gnulib](https://www.gnu.org/software/gnulib/)-style
[./bootstrap](https://www.gnu.org/software/gnulib/manual/html_node/Developer-tools.html)
script.

# Simple setup

Visit your GitLab project page and go to `Settings`, then `CI/CD` and
expand `General pipelines` and under `CI/CD configuration file` you
add the following:

```
jobs.yml@gnulib/pipeline
```

The disadvantage with this approach is that you cannot provide any
per-project variables or extensions.

# Overview

The [`jobs.yml`](jobs.yml) provides a simple entry-point template
suitable for a basic project.  It is not anticipated this will be
useful for real-world projects, but serves as inspiration on where to
start and should work for projects that have no external dependencies.
The file includes the [`defs.yml`](defs.yml) file and adds two
concrete jobs with system image names and job dependencies.  The
second file `defs.yml` provides re-usable code patterns.

# Custom project pipeline

We provide the following pipeline definitions for a select few
projects.  These files form the basis for discovering reusable
patterns that can be migrated to the generic `defs.yml` file above.
The string in the third column can be used in the GitLab CI/CD
pipeline configuration file setting field.

| Project | Job pipeline file | Project setting |
| ------- | ----------------- | --------------- |
| [GNU Networking Utilities](https://www.gnu.org/software/inetutils/) | [gnu/inetutils.yml](gnu/inetutils.yml) | gnu/inetutils.yml@gnulib/pipeline |

# Legal matters

See the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html] for the license.

```
Copyright (C) 2022-2024 Simon Josefsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
